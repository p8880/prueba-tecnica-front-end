import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { ComprasComponentComponent } from './components/compras-component/compras-component.component';
import { AppRoutingModule } from './app-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { CrudServicesService } from "./services/crud-services.service";

@NgModule({
  declarations: [
    AppComponent,
    ComprasComponentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [HttpClient, CrudServicesService],
  bootstrap: [AppComponent, ComprasComponentComponent]
})
export class AppModule { }
