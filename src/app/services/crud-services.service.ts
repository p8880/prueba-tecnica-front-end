import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";

const baseUrl = "http://localhost:8084/compras";


@Injectable({
  providedIn: 'root'
})
export class CrudServicesService {

  constructor(private http: HttpClient) { }
  create(data: any): Observable<any> {
    return this.http.post(baseUrl + "/crear", data);
  }
  getAll(): Observable<any> {
    return this.http.get(baseUrl + '/findAll');
  }
}
