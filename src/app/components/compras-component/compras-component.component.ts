import { Component, OnInit } from '@angular/core';
import { CrudServicesService } from "../../services/crud-services.service";

@Component({
  selector: 'app-compras-component',
  templateUrl: './compras-component.component.html',
  styleUrls: ['./compras-component.component.css']
})
export class ComprasComponentComponent implements OnInit {
  compra = {
    nombreCliente: "",
    tipoFacturacion: "",
    total_pagar: 0
  }

  producto = {
    idProducto: "",
    cantidad: 0
  }

  detalle = {
    idProducto: "",
    cantidad: ""
  }

  allCompras = [];

  submitted = false;

  constructor(private crud: CrudServicesService) { }

  ngOnInit(): void {
  }

  save(): void {
    const compra = {
      nombreCliente: this.compra.nombreCliente,
      tipoFacturacion: this.compra.tipoFacturacion,
      total_pagar: this.compra.total_pagar
    }
    const producto =[ {
      idProducto: this.producto.idProducto,
      cantidad: this.producto.cantidad
    }]

    //const detalleProducto =  this.getDetalleProductos();
    const dto = {
      id: 1,
      codeDetail: "2022",
      resquestDetail: "crear",
      ordenCompraEntity: compra,
      detalleEntity : producto
    }
    this.crud.create(dto).subscribe(
      response => {
        this.get();
        console.log(response);
        this.submitted = true;
      },
      error => {
        console.log("sucedio un error en servidor " + error);
      }
    )
  } 
  
  public getDetalleProductos(){
   return [{}];
  }

  get(): void {
    this.crud.getAll().subscribe(
      response => {
        this.allCompras = response.body;
      },
      error => {
        console.log(error);
      }
    );
  }


}
