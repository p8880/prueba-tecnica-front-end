import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from "@angular/router";
import { ComprasComponentComponent } from "./components/compras-component/compras-component.component";


const routes: Routes = [
  { path: "", redirectTo: "crear", pathMatch: "full" },
  { path: "crear", component: ComprasComponentComponent }
]


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
